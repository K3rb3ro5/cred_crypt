#ifndef TESTSECURESTRING_HPP
#define TESTSECURESTRING_HPP

#include <cstring> //strlen(), memcmp()
#include <string> //std::string class
#include "gtest/gtest.h" //testing::Test class
#include "src_header/secureString.hpp"

class unitTestSecureString : public ::testing::Test
{
    /* Setup and TearDown are unused */
};

#endif
