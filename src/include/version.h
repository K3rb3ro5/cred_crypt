#pragma once

/* This file is used to track the version of CredCrypt that is later written into th
 *  header
 */

#define VERSION_MAJOR 0.7
#define VERSION_MINOR 4
